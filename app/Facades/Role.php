<?php

namespace App\Facades;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

class Role
{
    public static function getList($param)
    {
        $role = DB::table('roles')
            ->select([
                'name',
            ])
            ->where('guard_name', 'api')
            ->orderBy($param['sort']['order_by'], $param['sort']['order']);

        Role::getListRenderFilter($role, $param);

        Role::getListRenderSearch($role, $param['search']);

        return $role;
    }

    protected static function getListRenderFilter(&$query, $param)
    {
        if (isset($param['page']) && $param['page'] !== null && isset($param['size']) && $param['size'] !== null) {
            $query->offset($param['page'])->limit($param['size']);
        }
    }

    protected static function getListRenderSearch(&$query, $search)
    {
        if (isset($search) && $search !== null && $search !== '') {
            $query->where(function ($q) use ($search) {
                $q->Where('name', 'LIKE', "%{$search}%")
                    ->orWhere('guard_name', 'LIKE', "%{$search}%");
            });
        }
    }

    public static function assignRoles($user_id, array $roles = [])
    {
        $user = UserModel::find($user_id);
        if (! $user) {
            return;
        }

        foreach ($roles as $role_name) {
            DB::table('roles')
                ->where('name', $role_name)
                ->get()
                ->each(function ($role) use ($user_id) {
                    DB::table('model_has_roles')
                        ->updateOrInsert([
                            'role_id' => $role->id,
                            'model_type' => config('permission.model_type'),
                            'model_id' => $user_id,
                        ]);
                });
        }
    }
}
