<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\ActivationCodeMail;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;

use Mail;
use Redirect;
use Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('admin.auth.login');
    }
 
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $login = [
            'email' => $request->email,
            'password' => $request->password
        ];

        $user = User::where('email', $request->email)->first(); 
         
        if ($user && Auth::attempt($login)) {
            return redirect('/admin/dashboard');
        }else{ 
            return Redirect::back()->withErrors(['message' => 'Login failed!, check your username and password.'])->withInput();
        }
    }

    public function logout()
    {
        if(Auth::check()) {
            Auth::logout();
            session()->flush();
        }

        return redirect('/admin/login');
    }
}
