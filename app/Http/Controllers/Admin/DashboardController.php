<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\ReportAnswer;

use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $navigator = [
            [
                'title' => 'Dashboard'
            ],
        ]; 
        
        return view('admin.dashboard.index', [
            'title' => 'Dashboard',
            'active' => 'dashboard', 
            'navigator' => $navigator, 
        ]);
    }
}
