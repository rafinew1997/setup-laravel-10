<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

use App\Models\Company;
use App\Models\User;

use DataTables;
use Exception;
use Hash; 

class UserController extends Controller
{
    public function index()
    {
        $navigator = [
            [
                'title' => 'User',
            ],
        ];
 
        return view('admin.user.index', [
            'title' => 'User',
            'active' => 'user',
            'navigator' => $navigator
        ]);
    }

    public function dt()
    {
        $users = DB::table('users')
            ->select([
                'users.*',   
                'companies.name as company_name',
            ])  
            ->leftJoin('companies', 'companies.id','=','users.company_id') 
            ->whereNull('users.deleted_at')
            ->orderBy('users.name', 'asc')
            ->get();

        return DataTables::of($users)->addIndexColumn()->make(true);
    }

    public function store(Request $request)
    { 
        try {  
            $user = User::create([ 
                'uuid' => Uuid::uuid4(),
                'name' => $request->name,
                'password'=> Hash::make($request->password),
                'email' => $request->email,    
                'company_id' => $request->company_id,   
            ]);

            return response([
                "status"    => 200,
                "data"      => $user,
                "message"   => 'Data Tersimpan'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        try { 

            $user = User::find($id);

            if ($request->password != null) {
                $user->update([  
                    'password' => Hash::make($request->password),  
                ]);
            }

            $user->update([ 
                'name' => $request->name, 
                'email' => $request->email,  
                'company_id' => $request->company_id, 
            ]);

            return response([
                "status"    => 200,
                "data"      => $user,
                "message"   => 'Data Terubah'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $user = User::find($id)->delete();

            return response([
                "status"=> 200,
                "data"  => $user,
                "message"   => 'Data Terhapus'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function import_client(Request $request)
    {    
        try {
            // reading file
            $file_name = $request->input('file');

            $company = Company::find(session()->get('company_id', null));

            $reader = substr($file_name, strpos($file_name, '.') + 1) === 'xls' ? new XlsReader() : new XlsxReader();
    
            $spreadsheet = $reader->load($request->file);
            $worksheet = $spreadsheet->getActiveSheet();

            // is valid format?
            $valid = true;
           
            if ($worksheet->getCellByColumnAndRow(1, 1)->getValue() !== 'No') $valid = false; 
            if ($worksheet->getCellByColumnAndRow(2, 1)->getValue() !== 'Employee Name') $valid = false;  
            if ($worksheet->getCellByColumnAndRow(3, 1)->getValue() !== 'Email Address') $valid = false;  
            if (!$valid) return response(['message' => 'File format is invalid'], 400);

            // looping row
            $row = 2;
            $employee_id = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
            while ($employee_id) {
             
                $check_email = User::where('email', $worksheet->getCellByColumnAndRow(6, $row)->getValue())->first(); 

                if (($check_code == null) && ($check_email == null)) { 
                    $gender;
                    if ($worksheet->getCellByColumnAndRow(4, $row)->getValue() == 'Male') {
                        $gender = '1';
                    }else{
                        $gender = '2';
                    } 
                    $client = User::updateOrCreate(
                        [ 
                            'email' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                        ],
                        [
                            'company_id' => session()->get('company_id', null),  
                            'name' => $worksheet->getCellByColumnAndRow(2, $row)->getValue(), 
                            'email' => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                            'is_active' => 1, 
                            'role_id' => 5,
                        ]
                    ); 
                } 

                $row++;
                $employee_id = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
            }
            return response([
                "status"    => 200,
                // "data"      => $client,
                "message"   => 'Data Tersimpan'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }
}
