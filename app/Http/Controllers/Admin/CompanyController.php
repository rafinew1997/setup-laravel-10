<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Company;

use DataTables;
use Hash; 

class CompanyController extends Controller
{
    public function index()
    {
        $navigator = [
            [
                'title' => 'Company',
            ],
        ];
 
        return view('admin.company.index', [
            'title' => 'Company',
            'active' => 'company',
            'navigator' => $navigator
        ]);
    }

    public function dt()
    {
        $companies = DB::table('companies')
            ->select([
                'companies.*', 
            ]) 
            ->whereNull('companies.deleted_at')
            ->orderBy('companies.name', 'asc')
            ->get();


        return DataTables::of($companies)->addIndexColumn()->make(true);
    }

    public function store(Request $request)
    { 
        try {  
            $company = Company::create([ 
                'name' => $request->name,
                'type' => $request->type, 
            ]);

            return response([
                "status"    => 200,
                "data"      => $company,
                "message"   => 'Data Tersimpan'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        try { 

            $company = Company::find($id);

            $company->update([ 
                'name' => $request->name,
                'type' => $request->type, 
            ]);

            return response([
                "status"    => 200,
                "data"      => $company,
                "message"   => 'Data Terubah'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }

    public function destroy($id)
    {
        try {
            $company = Company::find($id)->delete();

            return response([
                "status"=> 200,
                "data"  => $company,
                "message"   => 'Data Terhapus'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }
}
