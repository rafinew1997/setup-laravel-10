<?php

namespace App\Http\Controllers;

use App\Facades\Role as RoleFacades;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class RoleController extends Controller
{
    public function list(Request $request)
    {
        try {
            $page = $request->page != 0 ? $request->page - 1 : 0;
            $start = $page * $request->size;

            $param = [
                'page' => $start,
                'size' => $request->size,
                'search' => $request->search,
                'sort' => $request->sort,
            ];

            $roles = RoleFacades::getList($param)
                ->get();

            if (! $roles) {
                $result = [
                    'status' => Response::HTTP_NOT_FOUND,
                    'message' => 'role Not Found',
                    'data' => null,
                ];

                return response($result, Response::HTTP_NOT_FOUND);
            }

            $result = [
                'status' => Response::HTTP_OK,
                'message' => 'Success',
                'data' => $roles,
            ];

            return response($result, Response::HTTP_OK);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, server busy',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function assign(Request $request)
    {
        DB::beginTransaction();
        try {

            RoleFacades::assignRoles($request->user_id, $request->roles);

            DB::commit();

        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, server busy',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        $result = [
            'status' => Response::HTTP_OK,
            'message' => 'Success',
        ];

        return response($result, Response::HTTP_OK);
    }
}
