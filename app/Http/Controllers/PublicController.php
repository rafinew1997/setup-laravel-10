<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    public function get_companies()
    {
        try {
            $data = DB::table('companies')
                ->select([
                    'id',
                    'name'
                ])
                ->whereNull('deleted_at')
                ->get();

            return response([
                "status"    => 200,
                "data"      => $data,
                "message"   => 'OK'
            ], 200);
        } catch (Exception $e) {
            return response([
                "status" => 400,
                "message"=> $e->getMessage(),
            ]);
        }
    }
  
}
