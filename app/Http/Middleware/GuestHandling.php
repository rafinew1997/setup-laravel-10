<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Request;

class GuestHandling
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2){
                return redirect('/admin/dashboard');
            } else if (Auth::user()->status_fill){
                return redirect('/client/step-finish');
            } else{
                return redirect('/client');
            }
        }else{
            return $next($request);
        }
    }
}
