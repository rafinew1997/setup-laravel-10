<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',  
    ];

    protected $dates = ['deleted_at'];
    
    public function users()
    {
        return $this->hasMany(User::class);
    } 
}
