<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyQuestionListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_question_lists', function (Blueprint $table) {
            $table->id();  
            $table->unsignedBigInteger('survey_id')->foreign()->references('id')->on('surveys')->nullable(); 
            $table->unsignedBigInteger('questions_id')->foreign()->references('id')->on('questions')->nullable(); 
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_question_lists');
    }
}
