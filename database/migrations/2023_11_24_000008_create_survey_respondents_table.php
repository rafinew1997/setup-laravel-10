<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyRespondentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_respondents', function (Blueprint $table) {
            $table->id();  
            $table->unsignedBigInteger('user_id')->foreign()->references('id')->on('users')->nullable(); 
            $table->unsignedBigInteger('survey_session_id')->foreign()->references('id')->on('survey_sessions')->nullable(); 
            $table->string('number_phone')->nullable()->default(null);
            $table->string('birthday')->nullable()->default(null);
            $table->string('gender')->nullable()->default(null);
            $table->datetime('datetime');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_respondents');
    }
}
