<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('question_in');
            $table->string('question_en');
            $table->string('description')->nullable()->default(null);
            $table->unsignedBigInteger('question_type_id')->foreign()->references('id')->on('question_types')->nullable(); 
            $table->unsignedBigInteger('question_category_id')->foreign()->references('id')->on('question_categories')->nullable(); 
            $table->boolean('other_option')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
