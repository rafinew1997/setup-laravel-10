<?php

namespace Database\Seeders;
 
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Spatie\Permission\Models\Role;
use Symfony\Component\Console\Output\ConsoleOutput;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // employee_accounts
        $employee_accounts = [
            [
                'user' => [
                    'name' => 'devee',
                    'email' => 'developer@gmail.com',
                ],
                'roles' => 'developer',
            ], 
        ];

        collect($employee_accounts)->each(function ($account) {
            $output = new ConsoleOutput();

            $output->writeln('<info>'.$account['user']['name'].'</info>');

            $user = User::updateOrCreate(
                ['name' => $account['user']['name']],
                array_merge(
                    $account['user'],
                    [
                        'password' => Hash::make(strtolower('password')),
                        'uuid' => Uuid::uuid4(),
                    ]
                )
            );

            // if ($account['roles']) {
            //     $user->assignRole(Role::where('name', $account['roles'])->get());
            // }
        });
    }
}
