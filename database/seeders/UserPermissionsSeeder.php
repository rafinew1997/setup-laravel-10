<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Symfony\Component\Console\Output\ConsoleOutput;

class UserPermissionsSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');

        // roles
        $role_apis = [
            // developer
            'developer' => [
                'user page add',
                'user page profile',
            ], 
        ];

        $role_webs = [
            // developer
            'developer' => [
                'user page access',
                'user page add',
            ], 
        ];

        $roles = [
            'api' => $role_apis,
            'web' => $role_webs,
        ];

        // masih on going
        collect($roles)->each(function ($role_by_guards, $guard_name) {
            collect($role_by_guards)->each(function ($permissions, $role_name) use ($guard_name) {

                collect($permissions)->each(function ($permission_name) use ($role_name, $guard_name) {
                    $output = new ConsoleOutput();
                    $output->writeln('<comment>Seeding . . .</comment>');
                    $output->writeln('<info>Role:</info> '.$role_name);
                    $output->writeln('<info>Permission:</info> '.$permission_name);

                    $role = DB::table('roles')->where(['name' => $role_name, 'guard_name' => $guard_name])->exists()
                        ? Role::findByName($role_name, $guard_name)
                        : Role::create(['name' => $role_name, 'guard_name' => $guard_name]);

                    $permission_is_exists = DB::table('permissions')
                        ->where(['name' => $permission_name, 'guard_name' => $guard_name])
                        ->exists();

                    if ($permission_is_exists) {
                        $permission = Permission::findByName($permission_name, $guard_name);
                    } else {
                        $permission = Permission::create(['name' => $permission_name, 'guard_name' => $guard_name]);
                    }

                    $role->givePermissionTo($permission);
                });
            });
        });
    }
}
