<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $arr = [
            'Demo Company', 
        ];

        for ($i=0; $i < count($arr); $i++) {
            $company = \App\Models\Company::updateOrCreate([
                'name' => $arr[$i]
            ]); 
        }
    }
}
