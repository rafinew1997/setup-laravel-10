<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\AuthController; 
use App\Http\Controllers\Admin\DashboardController; 
use App\Http\Controllers\Admin\CompanyController; 
use App\Http\Controllers\Admin\UserController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::post('/store', [DepositController::class, 'store'])->middleware('permission:user page add,api');

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['guest-handling'])->group(function () { 

    Route::get('admin/login', [AuthController::class, 'index']);
    Route::post('admin/login', [AuthController::class, 'login']);
 
    // Route::get('login', [ClientAuthController::class, 'index'])->name('login');
});

Route::middleware(['auth-handling'])->group(function () {
    Route::prefix('/admin')->group(function () {
        Route::get('logout', [AuthController::class, 'logout']); 

        Route::get('dashboard', [DashboardController::class, 'index']);
 
        Route::resource('company', CompanyController::class);
        Route::post('company/dt', [CompanyController::class, 'dt']);
 
        Route::resource('user', UserController::class);
        Route::post('user/dt', [UserController::class, 'dt']); 

    }); 
});

//! Public
include base_path('routes/public.php');