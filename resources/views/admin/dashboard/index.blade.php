@extends('admin.layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css">
@endpush

@section('content') 
    
@endsection

@push('script')
    <script src="{{asset('assets/js/chart-lib/chart.js')}}"></script>
    <script src="{{asset('assets/js/chart-lib/chartjs-plugin-datalabels.js')}}"></script>
    <script src="https://cdn.tutorialjinni.com/chart.piecelabel.js/0.15.0/Chart.PieceLabel.min.js"></script> 
@endpush
