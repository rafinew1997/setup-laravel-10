<nav class="sidebar-main">
    <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
    <div id="sidebar-menu" style="margin-bottom: 100px">
        <ul class="sidebar-links" id="simple-bar" style="margin-bottom: 100px">
            <li class="back-btn">
                <div class="mobile-back text-end">
                    <span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i>
                </div>
            </li>
            <li class="sidebar-main-title">
                <div>
                    <h6>Dashboard</h6>
                    <p>Summary</p>
                </div>
            </li>
            <li class="sidebar-list"> 
                <a class="sidebar-link sidebar-title link-nav @if($active == 'dashboard') {{'active'}} @endif" href="{{url('admin/dashboard')}}">
                    <i data-feather="home"></i><span>Dashboard</span>
                </a>
            </li>
            <li class="sidebar-main-title">
                <div>
                    <h6>Data Processing</h6> 
                    {{-- <p>Assessment Data</p>  --}}
                </div>
            </li>
            <li class="sidebar-list">
                <a class="sidebar-link sidebar-title link-nav @if($active == 'recruitment') {{'active'}} @endif" href="{{url('admin/recruitment')}}">
                    <i data-feather="file-text"></i><span>menu</span>
                </a>
            </li> 
            <li class="sidebar-main-title">
                <div>
                    <h6>Master Data</h6> 
                    <p>Core data that must be filled in</p> 
                </div>
            </li>
            <li class="sidebar-list">
                <a class="sidebar-link sidebar-title link-nav @if($active == 'company') {{'active'}} @endif" href="{{url('admin/company')}}">
                    <i data-feather="briefcase"></i><span>Company</span>
                </a>
            </li>

            <li class="sidebar-list"> 
                <a class="sidebar-link sidebar-title link-nav @if($active =='user') {{'active'}} @endif" href="{{url('admin/user')}}">
 
                    <i data-feather="user"></i><span>User</span>
                </a>
            </li> 
 
            <li class="sidebar-list">
                <a class="sidebar-link sidebar-title link-nav @if($active == 'question') {{'active'}} @endif" href="{{url('admin/question')}}">
                    <i data-feather="list"></i><span>Question</span>
                </a>
            </li>

             
        </ul>
    </div>
    <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
</nav>
