<!-- Page Header Start-->
<div class="page-header">
    <div class="header-wrapper row m-0">
        <div class="header-logo-wrapper col-auto p-0]">
            <div class="logo-wrapper">
                <a href="/">
                    <img class="img-fluid for-light" src="{{asset('assets/images/logo/logo.png')}}" alt="">
                    <img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo.png')}}" alt="">
                </a>
            </div>
            <div class="toggle-sidebar">
                <i class="status_toggle middle sidebar-toggle" data-feather="align-center"></i>
            </div>
        </div>
        <div class="left-header col-8 horizontal-wrapper ps-0">
            @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) 
            <ul class="horizontal-menu"> 
                @php
                    $companies = \App\Models\Company::select(['id','name'])->get();

                    $company_id = session()->get('company_id', null);
                    $company_name = session()->get('company_name', 'All Company');

                    if(str_word_count($company_name) > 2){
                        $words = explode(" ", $company_name);
                        $acronym = "";

                        foreach ($words as $w) {
                            $acronym .= $w[0];
                        }
                    }else {
                        $acronym = $company_name;
                    }
                @endphp
                <li class="level-menu outside">
                    <a class="nav-link">
                        <i class="fa fa-building-o"></i>
                        <span>
                            {{$acronym}}
                        </span>
                    </a>
                    <ul class="header-level-menu menu-to-be-close" style="width: 225px;">
                        <li>
                            <a href="{{url('admin/set-company/0')}}">
                                <span>All Company</span>
                            </a>
                        </li>
                        @foreach ($companies as $company)
                        <li>
                            <a href="{{url('admin/set-company/'.$company->id)}}">
                                <span>{{$company->name}}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
            @endif
        </div>
        <div class="nav-right col pull-right right-header p-0">
            <ul class="nav-menus">
                <li>
                    <div class="mode"><i class="fa fa-moon-o"></i></div>
                </li>
                <li class="profile-nav onhover-dropdown p-0 me-0">
                    <div class="media profile-media">
                        <div class="media-body">
                            <span>{{Auth::user()->name}}</span>
                            <p class="mb-0 font-roboto">
                               {{-- {{Auth::user()->role->name}} --}}
                                <i class="middle fa fa-angle-down"></i>
                            </p>
                        </div>
                    </div>
                    <ul class="profile-dropdown onhover-show-div">
                        <li>
                            <a href="{{url('admin/logout')}}">
                                <i data-feather="log-in"> </i><span>Log out</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Page Header Ends -->
