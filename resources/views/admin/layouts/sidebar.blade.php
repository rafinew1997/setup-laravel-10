@php
    $segment2 = Request::segment(2);
    $segment3 = Request::segment(3);
    $segment4 = Request::segment(4);

    $branch = session()->get('branch', 1);
@endphp
<!-- Page Sidebar Start-->
<div class="sidebar-wrapper">
    <div class="mb-5">
        <div class="logo-wrapper" style="height: 81px;">
            <a href="{{url('/')}}">
                <img class="img-fluid for-light" src="{{asset('logo/logo.png')}}" style="margin-top: -26px; height: 70px;">
            </a>
            <div class="back-btn"><i class="fa fa-angle-left"></i></div>
            {{-- <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"></i></div> --}}
        </div>
        <div class="logo-icon-wrapper">
            <a href="{{url('/')}}">
                <img class="img-fluid" src="{{asset('assets/logo/logo.png')}}" alt="">
            </a>
        </div> 

        @if (Auth::user()->role_id == 3)
            @include('admin.layouts.menu.hr')  
        @else 
            @include('admin.layouts.menu.admin')  
        @endif
    </div>
</div>
<!-- Page Sidebar Ends-->
