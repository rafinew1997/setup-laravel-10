<div class="modal fade" id="modal_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="" autocomplete="off" id="form_company">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Company</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"> 
                    <div class="form-group mt-3">
                        <label>Name<span class="text-danger">*</span></label>
                        <input type="text" name="name" parsley-trigger="change" required placeholder="Name" required
                        class="form-control">
                    </div>   
   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Link Self Assessment</h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Link</label>
                    <input type="text" id="link" class="form-control" readonly>
                </div>
                <div id="detail-self-assessment" style="display: none;">
                    <hr>
                    Tanggal Pengisian : <span id="date-self-assessment">-</span>
                    <form action="" id="self-assessment">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Keluhan yang dirasakan (misalnya perubahan perilaku, gejala kesehatan, kondisi pola piker, emosi yang dirasakan, pihak yang terlibat, kronologi singkat/masa lalu, dan informasi lain yang signfikan untuk diketahui)<span class="text-danger">*</span></label>
                                    <textarea readonly name="q1" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Upaya yang sudah/sedang dilakukan atau bantuan yang pernah didapatkan dari keluhan saat ini<span class="text-danger">*</span></label>
                                    <textarea readonly name="q2" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Kendala dari pihak/faktor internal atau eksternal yang menghambat proses penyelesaian masalah/keluhan yang dirasakan<span class="text-danger">*</span></label>
                                    <textarea readonly name="q3" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Support system yang dimiliki saat ini (misalnya keluarga, teman, rekan kerja, komunitas, dan sebagainya) dan yang paling dekat/berpengaruh<span class="text-danger">*</span></label>
                                    <textarea readonly name="q4" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Aktivitas keseharian, pola/jenis pekerjaan, atau kesibukan sehari-hari saat ini<span class="text-danger">*</span></label>
                                    <textarea readonly name="q5" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 mt-3">
                                <div class="form-group">
                                    <label class="">Harapan setelah sesi konseling<span class="text-danger">*</span></label>
                                    <textarea readonly name="q6" required rows="3" class="form-control custom-radius pl-3"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
