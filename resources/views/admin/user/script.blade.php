<script type="text/javascript">
    var Page = function() {
        var _componentPage = function(){
            var init_table, displaySelect;

            $(document).ready(function() {
                initTable();
                formSubmit();
                initAction(); 
                initSelect();
            });

            const initTable = () => {
                init_table = $('#init-table').DataTable({
                    destroy: true,
                    processing: true,
                    responsive: true,
                    serverSide: true,
                    sScrollY: ($(window).height() < 700) ? $(window).height() - 200 : $(window).height() - 450,
                    ajax: {
                        type: 'POST',
                        url: "{{ url('admin/user/dt') }}",
                    },

                    columns: [
                        { data: 'DT_RowIndex' }, 
                        { data: 'name' },
                        { data: 'email' },  
                        { data: 'company_name' }, 
                        { defaultContent: '' }
                        ],
                    columnDefs: [
                        {
                            targets: 0,
                            searchable: false,
                            orderable: false,
                            className: "text-center"
                        },  
                        {
                            targets: -1,
                            searchable: false,
                            orderable: false,
                            className: "text-center",
                            data: "id",
                            render : function(data, type, full, meta) {
                                let btn_reset = '';

                                if(full.with_password){
                                    btn_reset = `<a title="Reset Password" class="btn-reset-password ml-2 text-danger" href="{{url('/admin/wellbeing/master/agent/reset-password')}}/${data}"><i class="fa fa-key"></i></a>`
                                }

                                return ` 
                                        <a title="Edit" class="btn-edit" href="{{url('/admin/user')}}/${data}"><i class="icofont icofont-edit"></i></a>
                                        @if (Auth::user()->role_id == 1) 
                                        <a title="Hapus" class="btn-delete ml-2 text-danger" href="{{url('/admin/user')}}/${data}"><i class="fa fa-trash"></i></a>
                                        @endif
                                        `
                            }
                        },
                    ],
                    order: [[1, 'asc']],
                    searching: true,
                    paging:true,
                    lengthChange:false,
                    bInfo:true,
                    dom: '<"datatable-header"><tr><"datatable-footer"ip>',
                    language: {
                        search: '<span>Search:</span> _INPUT_',
                        searchPlaceholder: 'Search.',
                        lengthMenu: '<span>Show:</span> _MENU_',
                        processing: '<div class="text-center"> <div class="spinner-border text-primary" role="status"> <span class="sr-only">Loading...</span> </div> </div>',
                    },
                });

                $('#search').on('keyup', function () {
                    init_table.search(this.value).draw();
                });

                $('#pageLength').on('change', function () {
                    init_table.page.len(this.value).draw();
                });
            },
            initAction = () => {
                $(document).on('click', '#add-btn', function(event){
                    event.preventDefault();
 
                    $('#form_user').trigger("reset");
                    $('#form_user').attr('action','{{url('admin/user')}}');
                    $('#form_user').attr('method','POST'); 
 
                    displaySelectCompany.set([])

                    showModal('modal_user');
                });

                $(document).on('click', '#import_client-btn', function(event){
                    event.preventDefault();

                    $('#import_client-form').trigger("reset");
                    $('#import_client-form').attr('action','{{url('admin/user/import-client')}}');
                    $('#import_client-form').attr('method','POST');

                    showModal('import_client-modal');
                });

                $(document).on('click', '.btn-edit', function(event){
                    event.preventDefault();

                    var data = init_table.row($(this).parents('tr')).data();

                    $('#form_user').trigger("reset");
                    $('#form_user').attr('action', $(this).attr('href'));
                    $('#form_user').attr('method','PUT');
 
                    $('#form_user').find('input[name="name"]').val(data.name); 
                    $('#form_user').find('input[name="email"]').val(data.email);    
                    displaySelectCompany.set([data.company_id])
 

                    showModal('modal_user');
                });

                $(document).on('click', '.btn-delete', function(event){
                    event.preventDefault();
                    var url = $(this).attr('href');

                    Swal.fire({
                        title: 'Hapus user?',
                        text: "user yang dihapus akan hilang permanen!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Hapus!',
                        cancelButtonText: 'Batal!',
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-danger ml-3',
                        buttonsStyling: false,
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: url,
                                type: 'DELETE',
                                dataType: 'json',
                            })
                            .done(function(res, xhr, meta) {
                                if (res.status == 200) {
                                    toastr.success(res.message, 'Success')
                                    init_table.draw(false);
                                }
                            })
                            .fail(function() {
                                toastr.error('Gagal!', 'Failed')
                            })
                            .always(function() { });
                        }
                    })
                });

                $(document).on('click', '.btn-reset-password', function(event){
                    event.preventDefault();
                    var url = $(this).attr('href');

                    Swal.fire({
                        title: 'Reset Password?',
                        text: "Password baru akan dikirimkan ke email terdaftar!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, Reset!',
                        cancelButtonText: 'Batal!',
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-danger ml-3 btn-cancel',
                        buttonsStyling: false,
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                url: url,
                                type: 'POST',
                                dataType: 'json',
                            })
                            .done(function(res, xhr, meta) {
                                if (res.status == 200) {
                                    toastr.success(res.message, 'Success')
                                    init_table.draw(false);
                                }
                            })
                            .fail(function() {
                                toastr.error('Gagal!', 'Failed')
                            })
                            .always(function() { });
                        }
                    })

                    $('.btn-cancel').css('margin-left', '15px')
                });
            },
            formSubmit = () => {
                $('#form_user').submit(function(event){
                    event.preventDefault();

                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: $(this).serialize(),
                    })
                    .done(function(res, xhr, meta) {
                        if (res.status == 200) {
                            toastr.success(res.message, 'Success')
                            init_table.draw(false);
                            hideModal('modal_user');
                        }
                    })
                    .fail(function(res, error) {
                        toastr.error(res.responseJSON.message, 'Gagal')
                    })
                    .always(function() {
                    });
                });

                $('#import-form').submit(function(event){
                    event.preventDefault();

                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: new FormData(this),
                        contentType: false,
                        processData: false,
                    })
                    .done(function(res, xhr, meta) {
                        if (res.status == 200) {
                            toastr.success(res.message, 'Success')
                            init_table.draw(false);
                            hideModal('import-modal');
                        }
                    })
                    .fail(function(res, error) {
                        toastr.error(res.responseJSON.message, 'Gagal')
                    })
                    .always(function() {
                    });
                });

                $('#replace-form').submit(function(event){
                    event.preventDefault();

                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: new FormData(this),
                        contentType: false,
                        processData: false,
                    })
                    .done(function(res, xhr, meta) {
                        if (res.status == 200) {
                            toastr.success(res.message, 'Success')
                            init_table.draw(false);
                            hideModal('replace-modal');
                        }
                    })
                    .fail(function(res, error) {
                        toastr.error(res.responseJSON.message, 'Gagal')
                    })
                    .always(function() {
                    });
                });
            },
            initSelect = () => {
                $.ajax({
                    url: "{{url('public/get-companies')}}",
                    type: 'GET',
                    dataType: 'json',
                })
                .done(function(res, xhr, meta) {
                    let element = '';
                    $.each(res.data, function(index, data){
                        element += `<option value="${data.id}">${data.name}</option>`
                    })

                    $('#select-companies').html(element);

                    displaySelectCompany = new SlimSelect({
                        select: '#select-companies',
                        placeholder: "Select Company"
                    })
                });
            }

            const showModal = function (selector) {
                $('#'+selector).modal('show')
            },
            hideModal = function (selector) {
                $('#'+selector).modal('hide')
            }

        };

        return {
            init: function(){
                _componentPage();
            }
        }

    }();

    document.addEventListener('DOMContentLoaded', function() {
        Page.init();
    });

</script>
