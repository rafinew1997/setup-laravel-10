<div class="modal fade" id="modal_user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="" autocomplete="off" id="form_user">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">User</h5>
                    <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"> 
                    <div class="form-group mt-3">
                        <label>Name<span class="text-danger">*</span></label>
                        <input type="text" name="name" parsley-trigger="change" required placeholder="Name" required
                        class="form-control">
                    </div>  
                    
                    <div class="form-group mt-3">
                        <label>Email<span class="text-danger">*</span></label>
                        <input type="text" name="email" parsley-trigger="change" required placeholder="Email" required
                        class="form-control">
                    </div> 

                    <div class="form-group mt-3">
                        <label>Password<span class="text-danger">*</span></label>
                        <input type="password" name="password" parsley-trigger="change" required placeholder="Password" required
                        class="form-control">
                    </div> 
                    
                    <div class="form-group mt-3">
                        <label>Company <span class="text-danger">*</span></label>
                        <select type="text" name="company_id" required id="select-companies"></select>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
 