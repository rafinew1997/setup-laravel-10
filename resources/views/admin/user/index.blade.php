@extends('admin.layouts.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-header d-flex justify-content-between mb-3">
                    <h4 class="">{{ isset($title) ? 'Table '.$title : 'Table' }}</h4>
                    <div class="d-flex justify-content-end">
                        <fieldset style="margin-right: 10px;">
                            <div class="form-group mr-3">
                                <select class="form-select" id="pageLength">
                                    <option value="10">10</option>
                                    <option value="15">15</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </fieldset>
                        <fieldset style="margin-right: 10px;">
                            <div class="form-group">
                                <input type="text" class="form-control" id="search" placeholder="Search">
                            </div>
                        </fieldset>  
                        <button type="button" id="add-btn" class="btn btn-primary waves-effect width-md waves-light">
                            <i class="fa fa-plus-circle"></i> Add
                        </button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-hover" id="init-table" width="100%">
                        <thead class="thead-light">
                            <tr>
                                <th width="5%">#</th> 
                                <th >Name</th>
                                <th width="20%">Email</th>  
                                <th >Company</th>  
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.user.modal')  
@endsection
 
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"> 
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js"></script><script>
    $('.dropify').dropify();
</script>
@include('admin.user.script')
@endpush
